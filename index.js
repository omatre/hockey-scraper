import WebScraper from './web-scraper.js';
import express from 'express';

const app = express();
const { PORT = 3000 } = process.env;

let scraper = new WebScraper('khl'); //Try norway, norway2, shl, nhl, khl etc.
scraper.init();

app.use(express.json());

app.get('/', (req, res) => {
    return res.json({
        data : scraper.getAll()
    });
});

app.get('/teams', (req, res) => {
    return res.json({
        data : scraper.getAllTeamNames()
    });
});

app.get('/:teamId', (req, res) => {
    const {teamId = 0} = req.params;

    return res.json({
        data: scraper.getTeamById(teamId)
    });
});

app.get('/players/:teamId', (req, res) => {
    const {teamId = 0} = req.params;
    
    return res.json({
        data: scraper.getPlayersByTeamId(teamId)
    });
});

app.get('/players', (req, res) => {
    let players = scraper.getAllPlayers();
    return res.json({
        data: players
    });
});

app.listen(PORT, 'localhost', () => {
    console.log(`Listening on localhost:${ PORT }`);
});