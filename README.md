# Hockey Scraper

This application scrapes all the teams and players of given hockey leagues. It also contains an api to get various data.

Available api calls:

- get('/') returns all data

- get('/teams') returns all team names

- get('/:teamId') returns link, name and players for one team given by id

- get('/players') returns one single array of all players

- get('/players/:teamId') returns goalies, defenders and forwards for one team given by id

{

    data: [

        link: string,

        name: string,

        players: [

            goalies: [

                string

            ],

            defenders: [

                string

            ],

            forwards: [

                string

            ]

        ]

    ]

}