import puppeteer from 'puppeteer';

export default class WebScraper {
    constructor(league) {
        this.puppeteer = puppeteer;
        this.LEAGUE = 'https://www.eliteprospects.com/league/' + league;
        this.LEAGUE_TEAMS = [];
        this.browser = null;
        this.page = null;
    }

    async init() {
        this.browser = await this.puppeteer.launch({ headless: false });
        this.page = await this.browser.newPage();

        await this.getTeams(this.LEAGUE)
            .then(d => this.LEAGUE_TEAMS = d)
            .catch(e => console.log(e));

        for (let i = 0; i < this.LEAGUE_TEAMS.length; i++) {
            await this.getPlayers(i)
                .then(d => this.LEAGUE_TEAMS[i]["players"] = d)
                .catch(e => console.log(e));
        }
        
        await this.browser.close();
    }

    getAll() {
        return this.LEAGUE_TEAMS;
    }

    getAllTeamNames() {
        let teams = [];
        for (let i = 0; i < this.LEAGUE_TEAMS.length; i++) {
            teams.push(this.LEAGUE_TEAMS[i]["name"]);
        }
        return teams;
    }

    getTeamById(id) {
        return this.LEAGUE_TEAMS[id];
    }

    getPlayersByTeamId(id) {
        let players = [];
        this.LEAGUE_TEAMS[id]["players"]["goalies"].forEach(element => players.push(element));
        this.LEAGUE_TEAMS[id]["players"]["defenders"].forEach(element => players.push(element));
        this.LEAGUE_TEAMS[id]["players"]["forwards"].forEach(element => players.push(element));
        return players;
    }

    getAllPlayers() {
        let players = [];
        for (let i = 0; i < this.LEAGUE_TEAMS.length; i++) {
            console.log(this.LEAGUE_TEAMS[i]["players"]);
            this.LEAGUE_TEAMS[i]["players"]["goalies"].forEach(element => players.push(element));
            this.LEAGUE_TEAMS[i]["players"]["defenders"].forEach(element => players.push(element));
            this.LEAGUE_TEAMS[i]["players"]["forwards"].forEach(element => players.push(element));
        }
        return players;
    }

    async getTeams(link) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.page.goto(link);
                resolve(await this.page.evaluate(() => {
                    const elGetLigaenRoasterHrefs = document.querySelectorAll("#average-physical-stats > div > div.table-wizard > table > tbody > tr > td.team.sorted > a");
                    const teams = [];
                    elGetLigaenRoasterHrefs.forEach(element => teams.push({
                        "link": element.getAttribute('href'),
                        "name": element.innerHTML,
                        "players": []
                    }));
                    return teams;
                }));
            } catch (error) {
                reject(error);
            }
        });
    }

    async getPlayers(teamId) {
        return new Promise(async (resolve, reject) => {
            try {
                await this.page.goto(this.LEAGUE_TEAMS[teamId]["link"]);
                resolve(await this.page.evaluate(() => {
                    const players = {
                        "goalies": [],
                        "defenders": [],
                        "forwards": []
                    }

                    document.querySelectorAll("#roster > div > div.table-wizard > table > tbody:nth-child(5) > tr > td.sorted > span > a:nth-child(1)")
                        .forEach(element => players["forwards"].push(element.innerHTML));
                    document.querySelectorAll("#roster > div > div.table-wizard > table > tbody:nth-child(3) > tr > td.sorted > span > a:nth-child(1)")
                        .forEach(element => players["goalies"].push(element.innerHTML));
                    document.querySelectorAll("#roster > div > div.table-wizard > table > tbody:nth-child(4) > tr > td.sorted > span > a:nth-child(1)")
                        .forEach(element => players["defenders"].push(element.innerHTML));
                    return players;
                }))
            } catch (error) {
                reject(error)
            }
        });
    }
}